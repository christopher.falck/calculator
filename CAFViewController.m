//
//  CAFViewController.m
//  Calculator
//
//  Created by Christopher Falck on 3/20/14.
//  Copyright (c) 2014 FalckTech. All rights reserved.
//

#import "CAFViewController.h"

@interface CAFViewController ()
@property (weak, nonatomic) IBOutlet UILabel *finalScreen;
@property (weak, nonatomic) IBOutlet UILabel *firstNumScreen;
@property (weak, nonatomic) IBOutlet UILabel *secondNumScreen;
@property (weak, nonatomic) IBOutlet UILabel *firstOperatorScreen;
@property (weak, nonatomic) IBOutlet UILabel *secondOperatorScreen;



@end

@implementation CAFViewController

-(void) masterController:(double)numPassed
{
    if (self.clearOnNextPress == YES) {
        [self resetDefaultValues];
        self.clearOnNextPress = NO;
    }
    
    
    if(self.screenSelector == 1 && self.decimalSelector == 1)
    {
        self.holdFirstNumScreen *= 10;
        self.holdFirstNumScreen += numPassed;
        [self updateScreen: self.holdFirstNumScreen];
    }
    
    if(self.screenSelector == 2 && self.decimalSelector == 1)
    {
        self.holdSecondNumScreen *= 10;
        self.holdSecondNumScreen += numPassed;
        [self updateScreen: self.holdSecondNumScreen];
    }
    
    if(self.screenSelector == 1 && self.decimalSelector == 0)
    {
        self.holdFirstNumScreen += [self updateDecimalPosition:numPassed];
        [self updateScreen: self.holdFirstNumScreen];
        self.afterDecimalPosition += 1;

    }
    
    if(self.screenSelector == 2 && self.decimalSelector == 0)
    {
        self.holdSecondNumScreen += [self updateDecimalPosition:numPassed];
        [self updateScreen: self.holdSecondNumScreen];
        self.afterDecimalPosition += 1;
    }
}

-(double) updateDecimalPosition: (double) digitToUpdate
{
    int i = 0;
    while (i < self.afterDecimalPosition)
    {
        digitToUpdate /= 10;
        i++;
    }
    return digitToUpdate;
}

-(void) updateScreen:(double)updateScreenWith
{
    if (self.screenSelector == 1) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @"%f", updateScreenWith];
        [[self firstNumScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 2) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @"%f", updateScreenWith];
        [[self secondNumScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 3) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @"%f", updateScreenWith];
        [[self finalScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 4) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @" +"];
        [[self firstOperatorScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 5) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @" -"];
        [[self firstOperatorScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 6) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @" x"];
        [[self firstOperatorScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 7) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @"  /"];
        [[self firstOperatorScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 8) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @" ="];
        [[self secondOperatorScreen] setText:updatedCalculatorScreen];
    }
    else if (self.screenSelector == 9) {
        NSString *updatedCalculatorScreen = [NSString stringWithFormat: @""];//for clearing labels
        [[self finalScreen] setText:updatedCalculatorScreen];
        [[self firstNumScreen] setText:updatedCalculatorScreen];
        [[self secondNumScreen] setText:updatedCalculatorScreen];
        [[self firstOperatorScreen] setText:updatedCalculatorScreen];
        [[self secondOperatorScreen] setText:updatedCalculatorScreen];

    }
}



- (IBAction)oneButton:(id)sender
{
    [self masterController:1];
}

- (IBAction)twoButton:(id)sender
{
    [self masterController:2];

}

- (IBAction)threeButton:(id)sender
{
    [self masterController:3];

}

- (IBAction)fourButton: (id)sender
{
    [self masterController:4];

}

- (IBAction)fiveButton: (id)sender
{
    [self masterController:5];

}

- (IBAction)sixButton: (id)sender
{
    [self masterController:6];

}

- (IBAction)sevenButton:(id)sender
{
    [self masterController:7];

}

- (IBAction)eightButton:(id)sender
{
    [self masterController:8];

}

- (IBAction)nineButton:(id)sender
{
    [self masterController:9];

}

- (IBAction)zeroButton:(id)sender
{
    [self masterController:0];

}

- (IBAction)decimal:(id)sender {
    self.decimalSelector = 0;
}


-(IBAction)add:(id)sender
{
    self.screenSelector = 4;
    [self updateScreen:0];
    self.screenSelector = 2;
    self.decimalSelector = 1;
    self.afterDecimalPosition = 1;
}

- (IBAction)divide:(id)sender {
    self.screenSelector = 7;
    [self updateScreen:0];
    self.screenSelector = 2;
    self.decimalSelector = 1;
    self.afterDecimalPosition = 1;
}

- (IBAction)multiply:(id)sender {
    self.screenSelector = 6;
    [self updateScreen:0];
    self.screenSelector = 2;
    self.decimalSelector = 1;
    self.afterDecimalPosition = 1;
}

- (IBAction)subtract:(id)sender {
    self.screenSelector = 5;
    [self updateScreen:0];
    self.screenSelector = 2;
    self.decimalSelector = 1;
    self.afterDecimalPosition = 1;
}

-(IBAction)clear:(id)sender
{
    [self resetDefaultValues];
}

- (IBAction)equals:(id)sender
{
    NSString* check1 = [NSString stringWithFormat: self.firstNumScreen.text];
    NSString* check2 = [NSString stringWithFormat: self.secondNumScreen.text];

    
    if ( [check1 isEqualToString:@""])
    {
        self.screenSelector = 1;
        self.holdFirstNumScreen = 0;
        [self updateScreen:0];
        
    }
    
    if ( [check2 isEqualToString:@""])
    {
        self.screenSelector = 2;
        self.holdSecondNumScreen = 0;
        [self updateScreen:0];
        self.firstOperatorScreen.text = @" +";
    }
    
    
    
    if([check1 isEqualToString:@""] && [check2 isEqualToString:@""])
    {
        self.screenSelector = 3;
        self.holdFinalScreenNumber = 0;
        [self updateScreen:0];
        return;
    }
    
    
    NSString* operator = [NSString stringWithFormat: self.firstOperatorScreen.text];
    self.screenSelector = 8;//print the = sign
    [self updateScreen:0];
    
    self.screenSelector = 3;//print the final num
    
    if ([operator characterAtIndex:1] == '+') {
        double finalNum = (self.holdFirstNumScreen + self.holdSecondNumScreen);
        [self updateScreen: finalNum];
    }
    else if ([operator characterAtIndex:1] == '-') {
        double finalNum = (self.holdFirstNumScreen - self.holdSecondNumScreen);
        [self updateScreen: finalNum];
    }
    else if ([operator characterAtIndex:1] == 'x') {
        double finalNum = (self.holdFirstNumScreen * self.holdSecondNumScreen);
        [self updateScreen: finalNum];
    }
    else if ([operator characterAtIndex:2] == '/') {
        double finalNum = (self.holdFirstNumScreen / self.holdSecondNumScreen);
        [self updateScreen: finalNum];
    }
    
    self.clearOnNextPress = YES; //next time a number is entered, clear all fields first in master controller
}

-(void) resetDefaultValues
{
    self.holdFirstNumScreen = 0;
    self.holdFinalScreenNumber = 0;
    self.holdSecondNumScreen = 0;
    self.leftOfDecimal = 0;
    self.rightOfDecimal = 0;
    self.screenSelector = 9;
    [self updateScreen:0];
    
    
    self.decimalSelector = 1;//set up the initial values for decimal position and after decimal
    self.screenSelector = 1;//
    self.afterDecimalPosition = 1;
    self.clearOnNextPress = NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self resetDefaultValues];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
