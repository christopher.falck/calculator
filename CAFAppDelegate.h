//
//  CAFAppDelegate.h
//  Calculator
//
//  Created by Christopher Falck on 3/20/14.
//  Copyright (c) 2014 FalckTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
